import { MiniProlog } from './MiniProlog';

type PredicateType = "CONST" | "VAR";

type Predicate = {
  name: string;
  type: PredicateType;
  args: string[];
};

type ClauseType = "CONST" | "COMPOSED";

type Clause = {
  type: ClauseType;
  relationName: string;
  relation: string[];
  dependsOn: Predicate[];
};

type Solution = {
  result: boolean;
  programIndex: number;
};

const isLowerCase = (s1: string): boolean => {
  return s1.charAt(0) === s1.toLowerCase().charAt(0);
}

const findSolution = (program: Clause[], query: Predicate, programIndex: number, variables: any): Solution => {
  let searchCriteria: any = {};
  let variableIndex: any = {};

  // build object with key: index of the constant value and value: constant value that must match
  for (let i = 0; i < query.args.length; ++i) {
    const index: string = i.toString();
    if(isLowerCase(<string>query.args[i])) {
      searchCriteria[index] = query.args[i];
    } else {
      variableIndex[index] = query.args[i];
    }
  }

  for(let j = programIndex; j < program.length; ++j) {
    let e = <Clause>program[j];
    if(e.type != "CONST") {
      continue;
    }

    let result: boolean = true;
    let i: number = 0;
    for (i = 0; i < e.relation.length; ++i) {
      const index: string = i.toString();
      if (searchCriteria[index] !== undefined) {
        if (searchCriteria[index] !== e.relation[i]) {
          result = false;
          break
        }
      }
    }

    if (result) {
      // fill variables found
      for (let j = 0; j < e.relation.length; ++j) {
        const index: string = j.toString();
        if (searchCriteria[index] === undefined) {
          const variableName = variableIndex[index];
          variables[variableName] = e.relation[j];
        }
      }

      return {
        result: true,
        programIndex: i + 1 // so if we have to search back we start in the next position
      };
    }
  }

  return {
    result: false,
    programIndex: program.length + 1 // we are indicating EOF
  };
};

const variablesAreEmpty = (variables: any): boolean => {
  for (const key in variables) {
    if (variables[key] !== "") {
      return false;
    }
  }

  return true;
};

const allVariablesAreFound = (variables: any): boolean => {
  for (const key in variables) {
    if (variables[key] === "") {
      return false;
    }
  }

  return true;
};

const getPredicatesWithVarNotFound = (predicates: Predicate[],variables: any): Predicate[] => {
  let variablesNotFound: any = {};
  for (const key in variables) {
    if (variables[key] === "") {
      variablesNotFound[key] = ""
    }
  }

  let predicatesWithVarNotFound: Predicate[] = [];
  predicates.forEach(e => {
    for (let i = 0; i < e.args.length; i++) {
      // @ts-ignore
      if(e.args[i] in variablesNotFound) {
        predicatesWithVarNotFound.push(e);
      }
    }
  })

  return predicatesWithVarNotFound;
};

const findSolutionForSet = (program: Clause[], predicates: Predicate[], variables: any): boolean => {
  // recursion base case
  if (predicates.length === 0) {
    return true;
  }

  if(!variablesAreEmpty(variables)) {
    // replace variables for solution found
    for (let i = 0; i < predicates.length; ++i) {
      const args = <string[]>predicates[i]?.args;

      const argsClone  = Object.assign([], args);

      for (let j = 0; j < args.length; j++) {
        const element = <string>args[j];
        if (element in variables) {
          if (variables[element] != "") {
            // @ts-ignore
            predicates[i].args[j] = variables[element];
          }
        }
      }

      let solution = findSolution(program, <Predicate>predicates[i], 0, variables);

      if (!solution.result) {
        // @ts-ignore
        // ToDo me parece que aca falta volver a clonar todo correctamente
        predicates[i].args = argsClone;
        return false
      }

      if (allVariablesAreFound(variables)) { // por aca falla, porque esta tomando grandparent y grandchild como variables a encontrar
        return  true;
      }

      predicates = getPredicatesWithVarNotFound(predicates, variables)
    }
  }

  const [p, ...subSetPredicate] = predicates;
  let res: Solution = {
    result: false,
    programIndex: 0,
  };
  let resultSubset: boolean = false;

  while (!resultSubset && res.programIndex < program.length) {
    res = findSolution(program, <Predicate>p, res.programIndex, variables);
    if (!res.result) {
      return false;
    }

    resultSubset = findSolutionForSet(program, subSetPredicate, variables)
  }

  return resultSubset;
}

const checkIfrelationMatchWithConstant = (e: Clause, query: Predicate) => {
  if (e.relation.length !== query.args.length) {
    return false;
  }

  let result: boolean = true;
  for (let i = 0; i < e.relation.length; ++i) {
    if (e.relation[i] !== query.args[i]) {
      result = false;
      break;
    }
  }

  return result;
};

const findVariables = (args: string[]): any => {
  let variables: any = {};
  for (let j = 0; j < args.length; ++j) {
    const element: string = <string>args[j];
    if(!isLowerCase(element)) {
      variables[element] = "";
    }
  }

  return variables;
};

const checkIfrelationMatchWithComposed = (program: Clause[], query: Predicate, e: Clause): boolean => {
  // 1. identify constants using a key-value with key the variableName, and value the constant
  let queryConst: any = {}
  for (let i = 0; i < e.relation.length; ++i) {
    const key: string = <string>e.relation[i];
    queryConst[key] = query.args[i];
  }

  // 2. replace on the dependsOn the constants, and take note wich are the variables
  let variables: any = {};
  // if the query is a variable we have to take note of the variables there
  if (query.type === "VAR") {
    query.args.forEach(element => {
      if(!isLowerCase(element)) {
        variables[element] = "";
      }
    })
  }

  let originalArgs: string[][] = [];
  for (let i = 0; i < e.dependsOn.length; ++i) { // [[GrandFather, Middle], [Middle, GrandChild]]
    // @ts-ignore
    originalArgs.push(Object.assign([], e.dependsOn[i].args))
    // @ts-ignore
    for (let j = 0; j < e.dependsOn[i].args.length; ++j) { // [GrandFather, Middle]
      const element: string = <string>e.dependsOn[i]?.args[j];
      if (element in queryConst) {
        // @ts-ignore
        e.dependsOn[i].args[j] = queryConst[element];
        continue;
      }

      if(!isLowerCase(element)) {
        variables[element] = "";
      }
    }
  }

  // 3. find if there is a possible solution for all the dependencies

  const result = findSolutionForSet(program, e.dependsOn, variables)

  // 4. restore variables
  for (let i = 0; i < e.dependsOn.length; ++i) {
    // @ts-ignore
    e.dependsOn[i].args = originalArgs[i]
  }

  return result;
};

const checkValidation = (program: Clause[], query: Predicate): boolean => {
  for(const e of program) {
    if(e.relationName == query.name) {
      if (e.type === "CONST") {
        const result = checkIfrelationMatchWithConstant(e, query);
        if (result) {
          return true
        }
      }

      if (e.type === "COMPOSED") {
        const result = checkIfrelationMatchWithComposed(program, query, e);
        if (result) {
          return true
        }
      }
    }
  }

  return false;
};

const isComposedPredicate = (program: Clause[], query: Predicate): boolean => {
  for(const e of program) {
    if(e.relationName == query.name) {
      if (e.type === "COMPOSED") {
        return true;
      }
    }
  }

  return false;
};

const miniProlog: MiniProlog<Clause, Predicate> = {
  buildPredicate: (name: string, ...args: string[]): Predicate => {
    let predicateType: PredicateType;
    const upperCaseFilter = args.filter(e => !isLowerCase(e));
    if(upperCaseFilter.length > 0) {
      predicateType = "VAR";
    } else {
      predicateType = "CONST";
    }

    return {
      'name': name,
      'type': predicateType,
      'args': args
    };
  },

  buildClause: (head: Predicate, ...body: Predicate[]): Clause => {
    const clause: Clause = {
      type: "CONST",
      relationName: "",
      relation: [],
      dependsOn: []
    }

    if(body.length > 0) {
      clause.type = "COMPOSED";
    } else {
      clause.type = "CONST";
    }

    clause.relationName = head.name;
    clause.relation = head.args;

    if (clause.type === "COMPOSED") {
      clause.dependsOn = body;
    }

    return clause;
  },

  canProve: (program: Clause[], query: Predicate): boolean => {



    if (query.type === "VAR") {
      if(!isComposedPredicate(program, query)) {
        let variables = findVariables(query.args);
        const {result, ...otherStuff} = findSolution(program,query, 0, variables);
        return result;
      }

      return checkValidation(program, query);
    } else {
      return checkValidation(program, query);
    }
  },
};

export default miniProlog;
